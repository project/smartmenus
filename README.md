# Smartmenus.js

This module provides Drupal integration with the Smartmenus.js advanced jQuery
website menu plugin. Mobile first, responsive and accessible list-based website
menus that work on all devices. This is the library that was included in core
for BackdropCMS, and is most commonly used with Bootstrap.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/smartmenus).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/smartmenus).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

-  [Smartmenus jQuery Plugin](https://www.smartmenus.org)


## Installation

1. Install the Smartmenus jQuery plugin.

  Smartmenus library is added using composer.

  Add this to your project's composer.json:

```json
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "drmonty/smartmenus",
                "version": "1.1.1",
                "type": "drupal-library",
                "dist": {
                    "url": "https://www.smartmenus.org/files/?file=smartmenus-jquery/smartmenus-1.1.1.zip",
                    "type": "zip"
                },
                "require": {
                    "composer/installers": "^1.2"
                }
            }
        }
    ],
```
2. Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to the blocks administration at _admin/structure/blocks_
2. Place the Responsive Main Menu (Smartmenus) block into a region
3. Select necessary options from the block configure form.
4. Check the UI.


## Maintainers

- Jen Lampton - [jenlampton](https://www.drupal.org/u/jenlampton)
- Doug Green - [douggreen](https://www.drupal.org/u/douggreen)
- Vishwa Chikate  - [vishwac9](https://www.drupal.org/u/vishwac9)
